<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import= "dao.*" %>
<%@ page import= "java.io.IOException"%>
<%@ page import= " java.io.PrintWriter"%>
<%@ page import = "java.util.*"%>
<%@ page import = "jakarta.servlet.http.HttpSession"%>


<!DOCTYPE html>	
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
<title>Insert title here</title>
</head>
<body>
<div class="container text-center">
<%
session = request.getSession(false);
DBDAO bdd = new DBDAO();
if (session != null) {
    session.invalidate(); // Ferme la session et supprime toutes les données associées
}
HttpSession sesion = request.getSession(true);
ArrayList<String> ref=new ArrayList<>();
String seek=request.getParameter("search");

ref=bdd.keepRef(seek);
if(ref == null){
	sesion.setAttribute("find", false);
	response.sendRedirect("index.jsp");
}
else{
	String fist=ref.get(0);
	if(fist.equals("N")) {
		sesion.setAttribute("find", false);
		response.sendRedirect("index.jsp");
	}
	else {
		sesion.setAttribute("find", true);
		
		if(ref.size()==1){
			String refe = ref.get(0);
			response.sendRedirect("CommuneFind.jsp?c="+refe);
		}
		else if (ref.size()>= 2){%>
		
	<h2>Voici La liste des Prélevements en rapport avec la donnee rentrée<a href="index.jsp" class="btn btn-secondary">Back</a></h2>
	
			<table class="table table-bordered">
			  <thead>
			    <tr>
			      <th scope="col">CdReseau</th>
			      <th scope="col">Commune</th>
			      <th scope="col">Date</th>
			      <th scope="col">Heure</th>
			      <th scope="col">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			  <% for(int i =0; i<(ref.size()); i++) {
				  		String cd = bdd.getCdReseau(ref.get(i));
						String sig = bdd.getCommune(ref.get(i));
						String c1 = bdd.getDate(ref.get(i));
						String refe = ref.get(i);
						String c2= bdd.getHeure(ref.get(i));%>
						<tr>
					      <th scope="row"><%=cd%></th>
					      <td><%=sig %></td>
					      <td><%=c1 %></td>
					      <td><%=c2 %></td>
					      <td><a href="CommuneFind.jsp?c=<%=refe%>"><button class="btn btn-lg btn-success" type="button">Consulter</button></a></td>
					    </tr>				    
	<%}%>   
	</tbody>
	</table><%}}
}

%>
</div>
</body>
</html>