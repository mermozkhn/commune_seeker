<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import= "dao.*" %>
<%@ page import= "java.io.IOException"%>
<%@ page import= "java.io.PrintWriter"%>
<%@ page import = "java.util.*"%>
<%@ page import = "dao.DBDAO"%>
<%@ page import = "model.User"%>
<%@ page import = "jakarta.servlet.http.HttpSession"%>

<%
HttpSession sessi = request.getSession(false);
Boolean fist =(Boolean) sessi.getAttribute("find");
if(sessi != null) {
	fist =(Boolean) sessi.getAttribute("find");
	
	if(fist!= null && fist==false) {
		response.sendRedirect("Logout");
	}
}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/leaflet.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/leaflet.css" />
<title>MonSite</title>
<style>
	   body{
	   position: relative;
	   }
	   
        body::before {
            content: '';
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('./back.jpg');
            background-size: cover;
            background-repeat: repeat;
            filter: blur(7px);
            z-index: -1;
        }

        .content {
            position: relative;
            z-index: 1;
            padding: 20px;
            background-color: #f0f0f0;
            text-align: center; /* Centrer le contenu horizontalement */
        }
        .search-input {
    padding: 8px 12px;
    font-size: 16px;
    border: 1px solid #ccc;
    border-radius: 4px;
    margin-right: 5px;
    width: 300px;
}
</style>
</head>
<body>
<div class="container content text-center">
<%
String refe = request.getParameter("c");
if(refe==null){
	response.sendRedirect("Logout");
}
else{
	DBDAO bdd = new DBDAO();
	List <User> lis=bdd.getPara(refe);
	
	%>
	<h2>Voici La liste des Prélevements en rapport avec la donnee rentrée<a href="Logout" class="btn btn-danger">Exit</a></h2>
	
			<table class="table table-bordered">
			  <thead>
			    <tr>
			      <th scope="col">Parametre</th>
			      <th scope="col">Rqana</th>
			      <th scope="col">limitequal</th>
			      <th scope="col">refqual</th>
			    </tr>
			  </thead>
			  <tbody>
			  <% for(User user : lis) {
				  		String cd = user.getLib();
						String sig = user.getRq();
						String c1 = user.getPap();
						String c2 = user.getChi();
						%>
						<tr>
					      <th scope="row"><%=cd%></th>
					      <td><%=sig %></td>
					      <td><%=c1 %></td>
					      <td><%=c2 %></td>
					    </tr>				    
	<%}%>   
	</tbody>
	</table>
	<%
}

%>

</div>

</body>
</html>