package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import model.User;


public class DBDAO{

	
	final String URL   = "jdbc:mysql://localhost:3306/bdd_jee_wil_mermoz";
	final String LOGIN = "root";
	final String PASS  = "root";
	Boolean connected = false;
	Connection conn= null;
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			}
	}
	

	private void dbConnect() {
		try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(URL, LOGIN, PASS); // Établissement de la connexion à la base de données
            System.out.println("Connexion à la base de données établie avec succès.");
            connected = true;
        } catch (ClassNotFoundException e) {
            System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
        } catch (SQLException e) {
            System.err.println("Erreur lors de la connexion à la base de données : " + e.getMessage());
        }
	}
	private void dbClose() {
		 if (conn != null) {
	            try {
	            	connected = false;
	                conn.close(); // Fermeture de la connexion à la base de données
	                System.out.println("Connexion à la base de données fermée avec succès.");
	            } catch (SQLException e) {
	            	e.printStackTrace();
	                System.err.println("Erreur lors de la fermeture de la connexion à la base de données : " + e.getMessage());
	            }
	           
	        }
	}
	
	public ArrayList<String> find(String searc, String sear) {
		dbConnect();
		ArrayList<String> list = new ArrayList<>();
		if (connected == true) {
		String query = "SELECT DISTINCT `Nom_de_la_commune` from bdd_jee_wil_mermoz.commune where `Code_postal` LIKE CONCAT('%', ?, '%') or `Nom_de_la_commune` LIKE CONCAT('%', ?, '%')";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, searc);
		ps.setString(2, sear);
		ResultSet rs = ps.executeQuery();
	
		if(rs.next()) {
			list.add(rs.getString("Nom_de_la_commune"));
			while(rs.next()) {
				list.add(rs.getString("Nom_de_la_commune"));
			}
		}
		else {
			list.add("N");
			
		}
		return list;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		list.add("N");
		return list;
		}
	
	public ArrayList<String> keepRef(String searc) {
		dbConnect();
		ArrayList<String> list = new ArrayList<>();
		if (connected == true) {
		String query = "SELECT `referencePrel` from bdd_jee_wil_mermoz.prelevement where `inseecommuneprinc`=?";
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, searc);
		ResultSet rs = ps.executeQuery();
		
		if(rs.next()) {
			list.add(rs.getString("referencePrel"));
			while(rs.next()) {
				list.add(rs.getString("referencePrel"));
			}
			return list;
		}
		else {
			list.add("N");
			return list;
		}
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		list.add("N");
		return list;
		}
	/**
	 * @return the country
	 */
	public String getCdReseau(String ref) {
		dbConnect();
		String resul ="";
		if (connected == true) {
		String query = "SELECT `cdreseau` from bdd_jee_wil_mermoz.prelevement where `referencePrel`=?";
		
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, ref);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			resul=rs.getString("cdreseau");
		}
		return resul;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return resul;

	}
	
	public String getCommune(String ref) {
		dbConnect();
		String resul ="";
		if (connected == true) {
		String query = "SELECT `nomcommuneprinc` from bdd_jee_wil_mermoz.prelevement where `referencePrel`=?";
		
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, ref);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			resul=rs.getString("nomcommuneprinc");
		}
		return resul;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return resul;

	}
	
	public String getDate(String ref) {
		dbConnect();
		String resul ="";
		if (connected == true) {
		String query = "SELECT `dateprelev` from bdd_jee_wil_mermoz.prelevement where `referencePrel`=?";
		
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, ref);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			resul=rs.getString("dateprelev");
		}
		return resul;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return resul;

	}
	
	public String getHeure(String ref) {
		dbConnect();
		String resul ="";
		if (connected == true) {
		String query = "SELECT `heureprelev` from bdd_jee_wil_mermoz.prelevement where `referencePrel`=?";
		
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, ref);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			resul=rs.getString("heureprelev");
		}
		return resul;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return resul;

	}
	
	public ArrayList<String> getParam(String ref) {
		dbConnect();
		ArrayList<String> list = new ArrayList<>();
		if (connected == true) {
		String query = "SELECT * from bdd_jee_wil_mermoz.prelevement where `referencePrel`=?";
		
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, ref);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			list.add(rs.getString("nomcommuneprinc"));
			list.add(rs.getString("dateprelev"));
			list.add(rs.getString("heureprelev"));
			list.add(rs.getString("plvconformitebacterio"));
			list.add(rs.getString("plvconformitechimique"));
			list.add(rs.getString("conclusionPrelev"));
			
		}
		else {
			list.add("N");
		}
		return list;

		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		list.add("N");
		return list;

	}
	
	public String getInsee(String ref) {
		dbConnect();
		String resul ="";
		if (connected == true) {
		String query = "SELECT DISTINCT `Code_commune_INSEE` from bdd_jee_wil_mermoz.commune where `Nom_de_la_commune`=?";
		
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, ref);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			resul=rs.getString("Code_commune_INSEE");
		}
		return resul;
		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return resul;

	}
	
	public List<User> getPara(String ref) {
		dbConnect();
		List<User> list = new ArrayList<>();
		if (connected == true) {
		String query = "SELECT * from bdd_jee_wil_mermoz.resultat where `referencePrel`=?";
		
		try {
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, ref);
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			String lib = rs.getString("libminparametre");
			String rq = rs.getString("rqana");
			String limit = rs.getString("limitequal");
			String refqual = rs.getString("refqual");
			User user = new User(rq,lib,limit,refqual);
			list.add(user);
			
		}
		return list;

		} catch (SQLException e) {
		e.printStackTrace();
		} finally {
		dbClose();
		}
		}
		return list;

	}
	
}
