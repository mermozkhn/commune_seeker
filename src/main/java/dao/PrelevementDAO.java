package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import model.Prelevement;

public class PrelevementDAO extends baseDAO {

	public PrelevementDAO(DataSource dataSource) {
		super(dataSource);
	}

	/**
	 * return all prelevement in a given commune with specific commune
	 * 
	 * @param inseeCommune
	 * @param cdReseau
	 * @return
	 * @throws Exception
	 */
	public List<Prelevement> getPrelevements(String inseeCommune, String cdReseau) throws Exception {

		List<Prelevement> allPrelevements = new ArrayList<>();

		Connection myCon = null;
		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			myCon = dataSource.getConnection();

			String sqlQuerry = "SELECT p.referencePrel, p.dateprelev, p.heureprelev, p.conclusionprelev, p.plvconformitebacterio, p.plvconformitechimique, p.plvconformitereferencebact, p.plvconformitereferencechim, p.inseecommuneprinc FROM prelevement p WHERE inseecommuneprinc=? and cdreseau=?";

			myStmt = myCon.prepareStatement(sqlQuerry);

			myStmt.setString(1, inseeCommune);

			myStmt.setString(2, cdReseau);

			myRs = myStmt.executeQuery();

			while (myRs.next()) {
				String refprelev = myRs.getString("referencePrel");
				String date = myRs.getString("DatePrelev");
				String heure = myRs.getString("heureprelev");
				String conclu = myRs.getString("conclusionprelev");
				String confbact = myRs.getString("plvconformitebacterio");
				String confchimq = myRs.getString("plvconformitechimique");
				String confrefbact = myRs.getString("plvconformitereferencebact");
				String confrefchimq = myRs.getString("plvconformitereferencechim");
				String inseecommune = myRs.getString("inseecommuneprinc");

				Prelevement tempPrelevement = new Prelevement(refprelev, date, heure, conclu, confbact, confchimq,
						confrefbact, confrefchimq, inseecommune);
				allPrelevements.add(tempPrelevement);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(myCon, myStmt, myRs);
		}

		return allPrelevements;

	}
	
	/**
	 * give all prelevement in a given departement
	 * @param codeDepartement
	 * @return
	 * @throws Exception
	 */
	public List<Prelevement> getPrelevements(String codeDepartement) throws Exception {

		List<Prelevement> allPrelevements = new ArrayList<>();

		Connection myCon = null;
		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			myCon = dataSource.getConnection();

			String sqlQuerry = "SELECT p.referencePrel, p.dateprelev, p.heureprelev, p.conclusionprelev, p.plvconformitebacterio, p.plvconformitechimique, p.plvconformitereferencebact, p.plvconformitereferencechim, p.inseecommuneprinc FROM prelevement p WHERE cddept=?";

			myStmt = myCon.prepareStatement(sqlQuerry);

			myStmt.setString(1, codeDepartement);

			myRs = myStmt.executeQuery();

			while (myRs.next()) {
				String refprelev = myRs.getString("referencePrel");
				String date = myRs.getString("DatePrelev");
				String heure = myRs.getString("heureprelev");
				String conclu = myRs.getString("conclusionprelev");
				String confbact = myRs.getString("plvconformitebacterio");
				String confchimq = myRs.getString("plvconformitechimique");
				String confrefbact = myRs.getString("plvconformitereferencebact");
				String confrefchimq = myRs.getString("plvconformitereferencechim");
				String inseecommune = myRs.getString("inseecommuneprinc");

				Prelevement tempPrelevement = new Prelevement(refprelev, date, heure, conclu, confbact, confchimq,
						confrefbact, confrefchimq, inseecommune);
				allPrelevements.add(tempPrelevement);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(myCon, myStmt, myRs);
		}

		return allPrelevements;

	}

}
