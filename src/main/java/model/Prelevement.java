package model;

public class Prelevement {
	private String referencePrel;
	private String DatePrelev;
	private String HeurePrelev;
	private String ConclusionPrelev;
	private String plvconformitebacterio;
	private String plvconformitechimique;
	private String plvconformitereferencebact;
	private String plvconformitereferencechim;
	private String idCommune;
	public String getReferencePrel() {
		return referencePrel;
	}
	public void setReferencePrel(String referencePrel) {
		this.referencePrel = referencePrel;
	}
	public String getDatePrelev() {
		return DatePrelev;
	}
	public void setDatePrelev(String datePrelev) {
		DatePrelev = datePrelev;
	}
	public String getHeurePrelev() {
		return HeurePrelev;
	}
	public void setHeurePrelev(String heurePrelev) {
		HeurePrelev = heurePrelev;
	}
	public String getConclusionPrelev() {
		return ConclusionPrelev;
	}
	public void setConclusionPrelev(String conclusionPrelev) {
		ConclusionPrelev = conclusionPrelev;
	}
	public String getPlvconformitebacterio() {
		return plvconformitebacterio;
	}
	public void setPlvconformitebacterio(String plvconformitebacterio) {
		this.plvconformitebacterio = plvconformitebacterio;
	}
	public String getPlvconformitechimique() {
		return plvconformitechimique;
	}
	public void setPlvconformitechimique(String plvconformitechimique) {
		this.plvconformitechimique = plvconformitechimique;
	}
	public String getPlvconformitereferencebact() {
		return plvconformitereferencebact;
	}
	public void setPlvconformitereferencebact(String plvconformitereferencebact) {
		this.plvconformitereferencebact = plvconformitereferencebact;
	}
	public String getPlvconformitereferencechim() {
		return plvconformitereferencechim;
	}
	public void setPlvconformitereferencechim(String plvconformitereferencechim) {
		this.plvconformitereferencechim = plvconformitereferencechim;
	}
	public String getIdCommune() {
		return idCommune;
	}
	public void setIdCommune(String idCommune) {
		this.idCommune = idCommune;
	}
	public Prelevement(String referencePrel, String datePrelev, String heurePrelev, String conclusionPrelev,
			String plvconformitebacterio, String plvconformitechimique, String plvconformitereferencebact,
			String plvconformitereferencechim, String idCommune) {
		super();
		this.referencePrel = referencePrel;
		DatePrelev = datePrelev;
		HeurePrelev = heurePrelev;
		ConclusionPrelev = conclusionPrelev;
		this.plvconformitebacterio = plvconformitebacterio;
		this.plvconformitechimique = plvconformitechimique;
		this.plvconformitereferencebact = plvconformitereferencebact;
		this.plvconformitereferencechim = plvconformitereferencechim;
		this.idCommune = idCommune;
	}

	
}
