package model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import dao.PrelevementDAO;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Prelevement;

/**
 * Servlet implementation class CommuneControllerServlet
 */
@WebServlet("/DepartementControllerServlet")
public class DepartementControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PrelevementDAO prelevementDAO;

	@Resource(name = "jdbc/bdd_jee_wil_mermoz")
	private DataSource dataSource;

	@Override
	public void init() throws ServletException {
		try {
			super.init();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		prelevementDAO = new PrelevementDAO(dataSource);
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DepartementControllerServlet() {
		super();
		// TODO Auto-generated constructor stub

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());

		// test d'accès aux prelevements d'un departement
		try {
			// prelevementDAO.getPrelevements("0100").size();
			System.out.print("");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}

		// test d'accès aux resultats d'un departement
		try {
			// resultatDAO.getResultatsByDepartment("076").size();
//			String astring = "076";
//			String string = "001";
//			String tring = "0010";
//			Integer.valueOf(astring);
//			Integer.valueOf(string);
//			Integer.valueOf(tring);
//			String newString = ""+Integer.valueOf(astring);
//			System.out.print("");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cdDept;
		int nbTestEchec;
		int nbTest;

		if (request.getParameter("dept").trim().length() == 1)
			cdDept = "00" + request.getParameter("dept").trim();
		else if (request.getParameter("dept").trim().length() == 2)
			cdDept = "0" + request.getParameter("dept").trim();
		else
			cdDept = request.getParameter("dept").trim();

		try {
			List<Prelevement> listPrelevement = prelevementDAO.getPrelevements(cdDept);
//			List<Resultat> listResultat = resultatDAO.getResultatsByDepartment(cdDept);
			nbTestEchec = nbTestEchec(listPrelevement);
			nbTest = nbTest(listPrelevement);
//			response.getWriter().println(nbTestEchec+" test echoues / " + nbTest +" effectues" );

			List<Prelevement> listPrelevementEchec = getTestEchec(listPrelevement);

			request.getSession().setAttribute("NbTestEchec", nbTestEchec);
			request.getSession().setAttribute("NbTest", nbTest);
			request.getSession().setAttribute("ListePrelevement", listPrelevementEchec);

			response.sendRedirect("Departement.jsp");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private int nbTestEchec(List<Prelevement> listPrelevement) {
		int count = 0;
		for (int i = 0; i < listPrelevement.size(); i++) {
			if (listPrelevement.get(i).getPlvconformitebacterio().trim().equals("N"))
				count++;
			if (listPrelevement.get(i).getPlvconformitechimique().trim().equals("N"))
				count++;
			if (listPrelevement.get(i).getPlvconformitereferencebact().trim().equals("N"))
				count++;
			if (listPrelevement.get(i).getPlvconformitereferencechim().trim().equals("N"))
				count++;
		}

		return count;
	}

	private int nbTest(List<Prelevement> listPrelevement) {
		return listPrelevement.size() * 4;
	}

	private List<Prelevement> getTestEchec(List<Prelevement> listPrelevement) {
		List<Prelevement> resultat = new ArrayList<>();
		for (int i = 0; i < listPrelevement.size(); i++) {
			if (listPrelevement.get(i).getPlvconformitebacterio().trim().equals("N"))
				resultat.add(listPrelevement.get(i));
			else if (listPrelevement.get(i).getPlvconformitechimique().trim().equals("N"))
				resultat.add(listPrelevement.get(i));
			else if (listPrelevement.get(i).getPlvconformitereferencebact().trim().equals("N"))
				resultat.add(listPrelevement.get(i));
			else if (listPrelevement.get(i).getPlvconformitereferencechim().trim().equals("N"))
				resultat.add(listPrelevement.get(i));
		}
		return resultat;
	}

}
