package model;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.ArrayList;

import dao.DBDAO;

/**
 * Servlet implementation class Finding
 */
public class Finding extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Finding() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DBDAO bdd = new DBDAO();
		HttpSession sesion = request.getSession(true);
		String seek=request.getParameter("search");
		ArrayList<String> insee = new ArrayList<>();
		insee=bdd.find(seek, seek);
		String fist=insee.get(0);
		int siz = insee.size();
		if(fist.equals("N")) {
			sesion.setAttribute("find", false);
			response.sendRedirect("index.jsp");
		}
		else {
			if(siz>=2){
				sesion.setAttribute("find", true);
				sesion.setAttribute("seek", seek);
				response.sendRedirect("index.jsp");
			}
			else if(siz==1) {
				String inse = bdd.getInsee(insee.get(0));
				response.sendRedirect("SearchCommune.jsp?search="+inse);
			}
		}
			

		}

		
}
