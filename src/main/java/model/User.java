package model;

public class User {

	String rq;
	String lib;
	String pap;
	String chi;
	/**
	 * @param rq
	 * @param lib
	 * @param pap
	 * @param chi
	 */
	public User(String rq, String lib, String pap, String chi) {
		super();
		this.rq = rq;
		this.lib = lib;
		this.pap = pap;
		this.chi = chi;
	}
	/**
	 * @return the rq
	 */
	public String getRq() {
		return rq;
	}
	/**
	 * @param rq the rq to set
	 */
	public void setRq(String rq) {
		this.rq = rq;
	}
	/**
	 * @return the lib
	 */
	public String getLib() {
		return lib;
	}
	/**
	 * @param lib the lib to set
	 */
	public void setLib(String lib) {
		this.lib = lib;
	}
	/**
	 * @return the pap
	 */
	public String getPap() {
		return pap;
	}
	/**
	 * @param pap the pap to set
	 */
	public void setPap(String pap) {
		this.pap = pap;
	}
	/**
	 * @return the chi
	 */
	public String getChi() {
		return chi;
	}
	/**
	 * @param chi the chi to set
	 */
	public void setChi(String chi) {
		this.chi = chi;
	}
	
}
